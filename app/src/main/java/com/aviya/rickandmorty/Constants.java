package com.aviya.rickandmorty;

public class Constants {
    public static final String BASE_URL = "https://www.rickandmortyapi.com/";
    public static final String GET_CHARACTERS = "api/character/";
}
