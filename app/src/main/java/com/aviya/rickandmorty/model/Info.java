package com.aviya.rickandmorty.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Info {
    private long count;
    private long pages;
    private String next;
    private String prev;

    @JsonProperty("count")
    public long getCount() { return count; }
    @JsonProperty("count")
    public void setCount(long value) { this.count = value; }

    @JsonProperty("pages")
    public long getPages() { return pages; }
    @JsonProperty("pages")
    public void setPages(long value) { this.pages = value; }

    @JsonProperty("next")
    public String getNext() { return next; }
    @JsonProperty("next")
    public void setNext(String value) { this.next = value; }

    @JsonProperty("prev")
    public String getPrev() { return prev; }
    @JsonProperty("prev")
    public void setPrev(String value) { this.prev = value; }
}
