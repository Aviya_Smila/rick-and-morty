package com.aviya.rickandmorty.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RickAndMortyCharacters {
    private Info info;
    private Character[] characters;

    @JsonProperty("info")
    public Info getInfo() { return info; }
    @JsonProperty("info")
    public void setInfo(Info value) { this.info = value; }

    @JsonProperty("results")
    public Character[] getCharacters() { return characters; }
    @JsonProperty("results")
    public void setCharacters(Character[] value) { this.characters = value; }
}
