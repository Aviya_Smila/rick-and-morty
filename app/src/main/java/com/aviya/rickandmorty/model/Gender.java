package com.aviya.rickandmorty.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import java.io.IOException;
import java.io.Serializable;

public enum Gender implements Serializable {
    FEMALE, MALE, UNKNOWN;

    @JsonValue
    public String toValue() {
        switch (this) {
            case FEMALE: return "Female";
            case MALE: return "Male";
            case UNKNOWN: return "unknown";
        }
        return null;
    }

    @JsonCreator
    public static Gender forValue(String value) throws IOException {
        if (value.equals("Female")) return FEMALE;
        if (value.equals("Male")) return MALE;
        if (value.equals("unknown")) return UNKNOWN;
        throw new IOException("Cannot deserialize Gender");
    }
}
