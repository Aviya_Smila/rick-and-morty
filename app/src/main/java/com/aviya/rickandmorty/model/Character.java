package com.aviya.rickandmorty.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.time.OffsetDateTime;

public class Character implements Serializable {
    private long id;
    private String name;
    private Status status;
    private Species species;
    private String type;
    private Gender gender;
    private Location origin;
    private Location location;
    private String image;
    private String[] episode;
    private String url;
    private OffsetDateTime created;

    @JsonProperty("id")
    public long getID() { return id; }
    @JsonProperty("id")
    public void setID(long value) { this.id = value; }

    @JsonProperty("name")
    public String getName() { return name; }
    @JsonProperty("name")
    public void setName(String value) { this.name = value; }

    @JsonProperty("status")
    public Status getStatus() { return status; }
    @JsonProperty("status")
    public void setStatus(Status value) { this.status = value; }

    @JsonProperty("species")
    public Species getSpecies() { return species; }
    @JsonProperty("species")
    public void setSpecies(Species value) { this.species = value; }

    @JsonProperty("type")
    public String getType() { return type; }
    @JsonProperty("type")
    public void setType(String value) { this.type = value; }

    @JsonProperty("gender")
    public Gender getGender() { return gender; }
    @JsonProperty("gender")
    public void setGender(Gender value) { this.gender = value; }

    @JsonProperty("origin")
    public Location getOrigin() { return origin; }
    @JsonProperty("origin")
    public void setOrigin(Location value) { this.origin = value; }

    @JsonProperty("location")
    public Location getLocation() { return location; }
    @JsonProperty("location")
    public void setLocation(Location value) { this.location = value; }

    @JsonProperty("image")
    public String getImage() { return image; }
    @JsonProperty("image")
    public void setImage(String value) { this.image = value; }

    @JsonProperty("episode")
    public String[] getEpisode() { return episode; }
    @JsonProperty("episode")
    public void setEpisode(String[] value) { this.episode = value; }

    @JsonProperty("url")
    public String getURL() { return url; }
    @JsonProperty("url")
    public void setURL(String value) { this.url = value; }

    @JsonProperty("created")
    public OffsetDateTime getCreated() { return created; }
    @JsonProperty("created")
    public void setCreated(OffsetDateTime value) { this.created = value; }
}
