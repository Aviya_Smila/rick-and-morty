package com.aviya.rickandmorty.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import java.io.IOException;
import java.io.Serializable;

public enum Status implements Serializable {
    ALIVE, DEAD, UNKNOWN;

    @JsonValue
    public String toValue() {
        switch (this) {
            case ALIVE: return "Alive";
            case DEAD: return "Dead";
            case UNKNOWN: return "unknown";
        }
        return null;
    }

    @JsonCreator
    public static Status forValue(String value) throws IOException {
        if (value.equals("Alive")) return ALIVE;
        if (value.equals("Dead")) return DEAD;
        if (value.equals("unknown")) return UNKNOWN;
        throw new IOException("Cannot deserialize Status");
    }
}
