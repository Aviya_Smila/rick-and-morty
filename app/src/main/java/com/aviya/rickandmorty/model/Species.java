package com.aviya.rickandmorty.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import java.io.IOException;
import java.io.Serializable;

public enum Species implements Serializable {
    ALIEN, HUMAN,UNKNOWN;

    @JsonValue
    public String toValue() {
        switch (this) {
            case ALIEN: return "Alien";
            case HUMAN: return "Human";
            case UNKNOWN:return "Unknown";
        }
        return null;
    }

    @JsonCreator
    public static Species forValue(String value) throws IOException {
        if (value.equals("Alien")) return ALIEN;
        else if (value.equals("Human")) return HUMAN;
        else return UNKNOWN; //(value.equals("Unknown"))

        //throw new IOException("Cannot deserialize Species");
    }
}
