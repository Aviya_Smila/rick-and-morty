package com.aviya.rickandmorty.model;

import com.aviya.rickandmorty.Constants;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface RickAndMortyAPI {
    @GET(Constants.GET_CHARACTERS)
    Observable<RickAndMortyCharacters> listCharacters(@Query("page") int param1);
}