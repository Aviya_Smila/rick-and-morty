package com.aviya.rickandmorty.views.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import com.aviya.rickandmorty.model.Character;
import com.aviya.rickandmorty.R;
import com.aviya.rickandmorty.databinding.CharacterRowTableBinding;
import com.aviya.rickandmorty.modelview.CharacterItemViewModer;

import java.util.Collections;
import java.util.List;

public class CharacterAdapter extends RecyclerView.Adapter<CharacterAdapter.CharacterAdapterViewHolder> {

    private List<Character> characterList;

    public CharacterAdapter() {this.characterList = Collections.emptyList();}

    @Override
    public CharacterAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        CharacterRowTableBinding itemUserBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.character_row_table ,parent, false);
        return new CharacterAdapterViewHolder(itemUserBinding);
    }

    @Override
    public void onBindViewHolder(CharacterAdapterViewHolder holder, int position) {
        holder.bindCharacter(characterList.get(position));

    }

    @Override
    public int getItemCount() {
        return  characterList.size();
    }

    public void setCharacterList(List<Character> characterList) {
        this.characterList = characterList;
        notifyDataSetChanged();
    }

    public static class CharacterAdapterViewHolder extends RecyclerView.ViewHolder {

        CharacterRowTableBinding mItemCharacterBinding;

        public CharacterAdapterViewHolder(CharacterRowTableBinding itemUserBinding) {
            super(itemUserBinding.itemPeople);
            this.mItemCharacterBinding = itemUserBinding;
        }

        void bindCharacter(Character character){
            if(mItemCharacterBinding.getCharacterViewModel() == null){
                mItemCharacterBinding.setCharacterViewModel(new CharacterItemViewModer(character, itemView.getContext()));
            }
            else {
                mItemCharacterBinding.getCharacterViewModel().setCharacter(character);
            }
        }
    }
}