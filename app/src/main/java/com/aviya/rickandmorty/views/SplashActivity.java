package com.aviya.rickandmorty.views;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import com.aviya.rickandmorty.R;
import com.aviya.rickandmorty.RamApp;

public class SplashActivity extends AppCompatActivity {

    private static final String TAG = SplashActivity.class.getName();
    /** Duration of wait **/
        private final int SPLASH_DISPLAY_LENGTH = 2000;
    private TextView mAppVersion;
    private Runnable mRunnable = () -> {
        /* Create an Intent that will start the Main-Activity. */
        Intent mainIntent = new Intent(SplashActivity.this,MainActivity.class);
        SplashActivity.this.startActivity(mainIntent);
        SplashActivity.this.finish();
    };
    private Handler mHandler;
    /** Called when the activity is first created. */
        @Override
        public void onCreate(Bundle icicle) {
            super.onCreate(icicle);
            setContentView(R.layout.activity_splash);
            mAppVersion = findViewById(R.id.appversion);
            mAppVersion.setText(RamApp.APP_VERSION);
            /* New Handler to start the Main-Activity
             * and close this Splash-Screen after some seconds.*/
            mHandler = new Handler();
            mHandler.postDelayed(mRunnable, SPLASH_DISPLAY_LENGTH);
        }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG,"onDestroy called");
        mHandler.removeCallbacks(mRunnable);
    }
}