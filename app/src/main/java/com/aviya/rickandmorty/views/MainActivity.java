package com.aviya.rickandmorty.views;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import android.os.Bundle;
import android.util.Log;
import com.aviya.rickandmorty.R;
import com.aviya.rickandmorty.databinding.ActivityMainBinding;
import com.aviya.rickandmorty.helpers.RecyclerViewPaginator;
import com.aviya.rickandmorty.modelview.MainViewModel;
import com.aviya.rickandmorty.views.adapter.CharacterAdapter;
import java.util.Observable;
import java.util.Observer;

public class MainActivity extends AppCompatActivity implements  Observer {

    private ActivityMainBinding charactersActivityBinding;
    private MainViewModel mViewModel;
    RecyclerView mRecyclerView;
    RecyclerViewPaginator mRecyclerViewPaginator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initDataBinding();
        mRecyclerView = findViewById(R.id.charactersRecyclerView);
        setUpListOfCountriesView(charactersActivityBinding.charactersRecyclerView);
        setUpObserver(mViewModel);
    }

    private void initRecyclerViewPaginator() {
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerViewPaginator = new RecyclerViewPaginator(mRecyclerView) {
            @Override
            protected void loadMoreItems() {
                mViewModel.loadData();
            }

            @Override
            public int getTotalPageCount() {
                return mViewModel.getTotalPages();
            }

            @Override
            public boolean isLastPage() {
                return mViewModel.isLastPage();
            }

            @Override
            public boolean isLoading() {
                return mViewModel.isFetching();
            }

        };

        /* Add this paginator to the onScrollListener of RecyclerView  */
        mRecyclerView.addOnScrollListener(mRecyclerViewPaginator);
    }

    private void initDataBinding() {
        charactersActivityBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        mViewModel = new MainViewModel(this);
        charactersActivityBinding.setMainViewModel(mViewModel);
    }

    // set up the list of country with recycler view
    private void setUpListOfCountriesView(RecyclerView countryRecycler) {
        CharacterAdapter countryAdapter = new CharacterAdapter();
        countryRecycler.setAdapter(countryAdapter);
        countryRecycler.setLayoutManager(new LinearLayoutManager(this));
    }

    public void setUpObserver(Observable observable) {
        observable.addObserver(this);
    }

    @Override protected void onDestroy() {
        super.onDestroy();
        mViewModel.reset();
    }

    @Override
    public void update(Observable o, Object arg) {
        if(o instanceof MainViewModel) {
            CharacterAdapter characterAdapter = (CharacterAdapter) charactersActivityBinding.charactersRecyclerView.getAdapter();
            MainViewModel mainViewModel = (MainViewModel) o;
            characterAdapter.setCharacterList(mainViewModel.getCharacters());
            initRecyclerViewPaginator();
        }
    }
}
