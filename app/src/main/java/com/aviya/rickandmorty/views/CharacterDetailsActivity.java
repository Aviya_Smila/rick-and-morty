package com.aviya.rickandmorty.views;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.aviya.rickandmorty.R;
import com.aviya.rickandmorty.model.Character;
import com.bumptech.glide.Glide;

public class CharacterDetailsActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String EXTRA_CHARACTER = "EXTRA_CHARACTER";
    ImageView mCharacterView;
    Character mCharacter;

    public static Intent getIntent(Context context, Character character) {
        Intent intent = new Intent(context, CharacterDetailsActivity.class);
        intent.putExtra(EXTRA_CHARACTER, character);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_character_details);
        mCharacterView = findViewById(R.id.imageView);
        mCharacter = (Character) getIntent().getSerializableExtra(EXTRA_CHARACTER);
        Glide.with(this).load(mCharacter.getImage()).into(this.mCharacterView);
        ImageButton button = findViewById(R.id.back_button);
        button.setOnClickListener(this);
        TextView textView = findViewById(R.id.toolbar_title);
        textView.setText(mCharacter.getName());
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.back_button:
                onBackPressed();
                break;
        }
    }
}
