package com.aviya.rickandmorty.modelview;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableInt;
import com.aviya.rickandmorty.Constants;
import com.aviya.rickandmorty.helpers.Utilities;
import com.aviya.rickandmorty.model.Character;
import com.aviya.rickandmorty.model.Converter;
import com.aviya.rickandmorty.model.RickAndMortyAPI;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.jackson.JacksonConverterFactory;


public class MainViewModel extends Observable {
    public static final int PAGE_START = 1;

    private final Retrofit API = new Retrofit.Builder().baseUrl(Constants.BASE_URL)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(JacksonConverterFactory.create(Converter.getMapper()))
            .build();

    public ObservableBoolean isLoading;
    public ObservableInt charactersRecycler;
    public ObservableInt progressBar;
    private int pageNumber;
    private ArrayList<Character> characters;
    private Context context;
    public CompositeDisposable compositeDisposable = new CompositeDisposable();
    private boolean isLastPage,isFetching;
    private int totalPages;

    public MainViewModel(@NonNull Context context) {
        this.context = context;
        this.characters = new ArrayList<>();
        this.pageNumber = 1;
        initializeViews();
        fetchCharactersList();
    }

    private void initializeViews() {
        this.charactersRecycler = new ObservableInt(View.GONE);
        this.progressBar = new ObservableInt(View.GONE);
        this.isLoading = new ObservableBoolean(false);
    }

    private void fetchCharactersList() {
        if (!Utilities.isNetworkAvailable(context)) {
            Toast.makeText(context, "Internet Not available, please try again later", Toast.LENGTH_LONG).show();
            return;
        }

        if(pageNumber==1)
            this.isLoading.set(true);
        else
            this.progressBar.set(View.VISIBLE);
        this.charactersRecycler.set(View.GONE);
        isFetching = true;
        RickAndMortyAPI service = API.create(RickAndMortyAPI.class);
        setChanged();
        notifyObservers();
        Disposable disposable = service.listCharacters(this.pageNumber)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(rickAndMortyCharacters ->{
                    isFetching = false;
                    totalPages = (int) rickAndMortyCharacters.getInfo().getPages();
                    Character[] added = rickAndMortyCharacters.getCharacters();
                    characters.addAll(Arrays.asList(added));
                    if(pageNumber==1)
                        isLoading.set(false);
                    else {
                        this.progressBar.set(View.GONE);
                    }
                    this.charactersRecycler.set(View.VISIBLE);
                    Log.i("Characters","received from API");
                    setChanged();
                    notifyObservers();
                }, throwable -> {
                    isFetching = false;
                    Log.d("Error", Objects.requireNonNull(throwable.getMessage()));
                    isLoading.set(false);
                    charactersRecycler.set(View.GONE);
                });

        compositeDisposable.add(disposable);
    }

    public List<Character> getCharacters() {
        if (this.characters==null)
            return null;
        return characters;
    }

    private void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
        context = null;
    }

    public boolean isLastPage() {
        Log.i("Pages","Current page " + pageNumber + " Total Pages " + totalPages);
        return totalPages <= this.pageNumber;
    }

    public void loadData() {
        pageNumber += 1;
        fetchCharactersList();
    }

    public void onRefresh() {
        characters.clear();
        pageNumber = PAGE_START;
        isLastPage = false;
        fetchCharactersList();
    }

    public int getTotalPages() {
        return totalPages;
    }

    public boolean isFetching() {
        return isFetching;
    }
}
