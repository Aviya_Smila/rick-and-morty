package com.aviya.rickandmorty.modelview;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import androidx.databinding.BaseObservable;
import androidx.databinding.BindingAdapter;
import com.aviya.rickandmorty.model.Character;
import com.aviya.rickandmorty.model.Gender;
import com.aviya.rickandmorty.model.Species;
import com.aviya.rickandmorty.views.CharacterDetailsActivity;
import com.bumptech.glide.Glide;

public class CharacterItemViewModer  extends BaseObservable {

    private Character character;
    private Context context;

    public CharacterItemViewModer(Character character, Context context){
        this.character = character;
        this.context = context;
    }

    public String getImageURL() {
        return character.getImage();
    }

    public String getName(){
        return character.getName();
    }

    public Species getSpecies(){
        return character.getSpecies();
    }

    public Gender getGender(){
        return this.character.getGender();
    }

    // Loading Image using Glide Library.
    @BindingAdapter("imageUrl") public static void setImageUrl(ImageView imageView, String url){
        Log.i("CharacterItemViewModer","Getting pic");
        if (url.isEmpty() || url == null)
            return;
        Glide.with(imageView.getContext()).load(url).into(imageView);
    }


    public void onItemClick(View v){
        context.startActivity(CharacterDetailsActivity.getIntent(v.getContext(), character));
    }

    public void setCharacter(Character character) {
        this.character = character;
        notifyChange();
    }
}
