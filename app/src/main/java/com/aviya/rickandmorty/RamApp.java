package com.aviya.rickandmorty;

import android.app.Application;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;


public class RamApp extends Application {

    public static String APP_VERSION;

    @Override
    public void onCreate() {
        super.onCreate();
        getAppVersion();
    }


    private void getAppVersion() {
        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            APP_VERSION = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

}
